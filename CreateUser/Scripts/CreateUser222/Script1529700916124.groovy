import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://sds-katalon-2.azurewebsites.net/Account/Login?ReturnUrl=%2F')

// check if url is opened successfully, if so, three fields (username, password and login button) should be present
assert (WebUI.verifyElementPresent(findTestObject('Page_Login (1)/input_Email'), 0)  != null &&
WebUI.verifyElementPresent(findTestObject('Page_Login (1)/input_Password'), 0) != null &&
WebUI.verifyElementPresent(findTestObject('Page_Login /input_loginPage-button'), 0) != null): "Webpage hasn't been loaded properly"

// Input username and password to login SDS
WebUI.setText(findTestObject('Page_Login (1)/input_Email'), 'admin@synectmedia.com')
WebUI.setText(findTestObject('Page_Login (1)/input_Password'), 'Admin@1234')
WebUI.click(findTestObject('Page_Login /input_loginPage-button'))
//WebUI.sendKeys(findTestObject('Page_Login (1)/input_Password'), Keys.chord(Keys.ENTER))

//WebUI.click(findTestObject('Page_Synect Data Server (1)/a_Dev Tools'))
//
//WebUI.click(findTestObject('Page_Synect Data Server (1)/a_Swagger API'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/a_SecurityModuleApi'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/a_apiSecurityModuleRegister'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/a_apiSecurityModuleGetUsers'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/input_submit'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/code_  Username string  Passwo'))
//
//WebUI.doubleClick(findTestObject('Page_Swagger UI (1)/textarea_credentials'))
//
//WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{\n  "Username": "wai3",\n  "Password": "string",\n  "Token": "string"\n}')
//
//WebUI.doubleClick(findTestObject('Page_Swagger UI (1)/textarea_credentials'))
//
//WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{\n  "Username": "wai3",\n  "Password": "waiwai",\n  "Token": "string"\n}')
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/input_submit'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/a_apiSecurityModuleGetUsers'))
//
//WebUI.click(findTestObject('Page_Swagger UI (1)/a_apiSecurityModuleGetUsers'))
//
//WebUI.closeBrowser()

