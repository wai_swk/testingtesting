import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://sds-katalon-2.azurewebsites.net/Account/Login?ReturnUrl=%2F')

// check if url is opened successfully, if so, three fields (username, password and login button) should be present
assert ((WebUI.verifyElementPresent(findTestObject('Page_Login SDS/input_Email'), 3) != null) && 
		(WebUI.verifyElementPresent(findTestObject('Page_Login SDS/input_Password'), 3) != null)) && 
		(WebUI.verifyElementPresent(findTestObject('Page_Login SDS/input_loginPage-button'), 3) != null): 'Webpage hasn\'t been loaded properly'

println "URL loaded sucessfully"

// Input username and password to login SDS
WebUI.setText(findTestObject('Page_Login SDS/input_Email'), username)
WebUI.setText(findTestObject('Page_Login SDS/input_Password'), password)
WebUI.click(findTestObject('Page_Login SDS/input_loginPage-button'))

if (username != "admin@synectmedia.com" || password != "Admin@1234"){
	assert WebUI.verifyElementPresent(findTestObject('Page_Login SDS/ul_invalid login attempt.'), 3) != null: "Incorrect credentials received"
	
}


println('Login successfully')


