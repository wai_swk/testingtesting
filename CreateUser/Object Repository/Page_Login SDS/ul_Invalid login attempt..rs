<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_Invalid login attempt.</name>
   <tag></tag>
   <elementGuidId>e4abab56-a233-4d91-acb9-10a7682c6a8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;loginForm&quot;)/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;validation-summary-errors text-danger&quot;]/ul[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invalid login attempt.
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loginForm&quot;)/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;validation-summary-errors text-danger&quot;]/ul[1]</value>
   </webElementProperties>
</WebElementEntity>
