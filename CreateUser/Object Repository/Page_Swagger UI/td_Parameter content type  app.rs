<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Parameter content type  app</name>
   <tag></tag>
   <elementGuidId>406fd4e7-a20b-4eb7-87af-5bd6e04bda14</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				
				
				
				Parameter content type:

  application/json
  text/json
  application/xml
  text/xml
  application/x-www-form-urlencoded


</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/form[@class=&quot;sandbox&quot;]/table[@class=&quot;fullwidth&quot;]/tbody[@class=&quot;operation-params&quot;]/tr[1]/td[2]</value>
   </webElementProperties>
</WebElementEntity>
