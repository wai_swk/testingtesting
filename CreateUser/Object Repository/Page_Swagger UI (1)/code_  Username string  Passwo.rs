<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>code_  Username string  Passwo</name>
   <tag></tag>
   <elementGuidId>91bf2f24-d5bd-496e-9753-ea3445a5e794</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>code</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>json</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>{
  &quot;Username&quot;: &quot;string&quot;,
  &quot;Password&quot;: &quot;string&quot;,
  &quot;Token&quot;: &quot;string&quot;
}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/form[@class=&quot;sandbox&quot;]/table[@class=&quot;fullwidth&quot;]/tbody[@class=&quot;operation-params&quot;]/tr[1]/td[5]/span[@class=&quot;model-signature&quot;]/div[1]/div[1]/div[1]/div[@class=&quot;signature-container&quot;]/div[@class=&quot;snippet&quot;]/pre[1]/code[@class=&quot;json&quot;]</value>
   </webElementProperties>
</WebElementEntity>
