import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())


RunConfiguration.setExecutionSettingFile('/var/folders/w4/8cfzqj2x1814lfqk4rf0dpsm0000gn/T/Katalon/Test Cases/CreateUser/20180622_145115/execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/CreateUser', new TestCaseBinding('Test Cases/CreateUser', [:]), FailureHandling.STOP_ON_FAILURE , false)
    
